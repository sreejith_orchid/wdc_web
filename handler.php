<?php

/**
 * Author : Sreejith CJ
 */

define('API_ENDPOINT', 'http://localhost:8888/wdc_api/items');
define('API_TOKEN', 'Token: 56dhwiwocncgftqrae#%cbc*2$%(khets#$n12x&56');
/**
 * Improvement : Need to implement OOPs here
 */
if ($_POST['action'] == "create") {
    $item = $_POST['item'];
    $payload = json_encode(array( "item"=> $item ));

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, API_ENDPOINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(API_TOKEN));
    $result = curl_exec($ch);
    curl_close($ch);
    //Improvement: JSON should return the default list item details along with all the list items
    getListItems();
}
else {
    getListItems();
}

function getListItems(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, API_ENDPOINT);
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(API_TOKEN)
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    echo $result;
}