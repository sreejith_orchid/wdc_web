$(document).ready(function(){
    $.ajax({ url: "handler.php",
    type: "POST",
    dataType: 'json',
    data: {action: "load"},
    success: function(result){
        $selectControl = $('#list_items');
        $.each(result, function(){
        $selectControl.append($('<option />').val(this[0]).text(this[1]));
        });
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
        $('.error-section').css('display','block');
        $('.error-message').html(thrownError);
        }
    });
    $('#submit').click(function(){
        var txtInput = $('#item').val();
        $('.error-section').css('display','none');
        $('.error-message').html('');
        $('.success-section').css('display','none');
        $('.success-message').html('');

        if(txtInput === ''){
            $('.error-section').css('display','block');
            $('.error-message').html('Please enter value for item field.');
            return;
        }
        $request = $.ajax({url: "handler.php", 
        type: "POST",
        dataType: 'json',
        data: {item: txtInput,action: "create"},
        success: function(result){
            $selectControl = $('#list_items');
            $selectControl.empty();
            $.each(result, function(){
            $selectControl.append($('<option />').val(this[0]).text(this[1]));
            });
            $('#list_items option:contains(' + txtInput + ')').prop('selected',true);
            $('#item').val('');

            $('.success-section').css('display','block');
            $('.success-message').html('List updated');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $('.error-section').css('display','block');
            $('.error-message').html(thrownError);
        }
        });
    });
});